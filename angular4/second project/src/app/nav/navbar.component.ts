/**
 * Created by pzoss on 19-Jul-17.
 */
import {Component} from "@angular/core";
import {AuthService} from "../user/auth.service";
import {ISession} from "../events/shared/event.model";
import {EventService} from "../events/shared/event.service";

@Component({
  selector:'nav-bar',
  templateUrl:'navbar.component.html',
  styles:[
    `
      .navbar{margin-top: 10px}
      .nav.navbar-nav{font-size: 15px;color:  white !important}
      #searchForm{margin-right: 100px}
      @media (max-width: 1200px){#searchForm {display: none}}
      li > a.active { color: orangered}
      .btn-primary{background-color: orangered;border-color: transparent}
      .btn-primary:hover{background-color: orange}
    `
  ]
})

export class NavBarComponent{

  searchTerm: string="";
  foundSessions: ISession[];

  constructor(private auth:AuthService, private eventService:EventService){

  }

  searchSessions(searchTerm){
    this.eventService.searchSessions(searchTerm).subscribe(
      sessions =>{
        this.foundSessions = sessions;
        console.log(this.foundSessions);
      })
  }
}
