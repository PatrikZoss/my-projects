import { FourthAngularPage } from './app.po';

describe('fourth-angular App', () => {
  let page: FourthAngularPage;

  beforeEach(() => {
    page = new FourthAngularPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
