import {Routes} from '@angular/router'

import {
  EventsListComponent,
  EventDetailComponent,
  CreateEventComponent,
  EventRouteActivator,
  EventListResolver,
  CreateSessionComponent,
  CreateTableComponent,
} from './events/index'

import {Error404Component} from "./errors/404.component";
/**
 * Created by pzoss on 19-Jul-17.
 */
export const appRoutes:Routes = [
  {    path:'events/new',component:CreateEventComponent,canDeactivate:['canDeactivatecreateEvent']},
  {    path:'events',component:EventsListComponent, resolve:{events:EventListResolver}  },
  {    path:'events/:id',component: EventDetailComponent, canActivate:[EventRouteActivator]},
  {    path:'events/session/new', component: CreateSessionComponent },
  {    path:'404',component:Error404Component},
  {    path: 'table',component:CreateTableComponent},
  {    path:'',redirectTo:'/events',pathMatch:'full'},
  {    path:'user', loadChildren: 'app/user/user.module#UserModule'}
]
