 var score = 150;
 var top_score;
 $(document).ready(function() {
        
        $('.block').draggable({
            containment:'window',
            stack: '.block',
        snap: true,
        snapMode: 'outer',
        snapTolerance: 10,
        });

      $('#blockTray').on('mousedown', function () {
          $('#instruction').fadeOut('slow');
      });
    
    // Make blocks rotate 90 deg on each click
        var angle = 90;    

        $('.block').click(function() {
          //var a=document.getElementById("square");
          //var b=document.getElementById("parallelogram");
          //if(a.value=="" || b.value=='.block'){console.log(a.value);}
          //else{
            if($(this).attr('id')!='square' && $(this).attr('id')!='parallelogram'){
                $(this).css ({
                  '-webkit-transform': 'rotate(' + angle + 'deg)',
                   '-moz-transform': 'rotate(' + angle + 'deg)',
                     '-o-transform': 'rotate(' + angle + 'deg)',
                    '-ms-transform': 'rotate(' + angle + 'deg)'
                            });
            angle+=90;}
        });
        
});

var g=0;
 function showHelp() {
 
  if(g===0){
  var div = document.createElement('div');
  div.textContent = "Klikni na objekt a potiahni ho na plochu. Postupným ukladaním objektov k sebe vyskladaj štvorec.Ak na objekt iba krátko klikneš tak sa objekt otočí. Kosoštvorec a kosodlžník sú už v takej polohe, v akej budú umiestnené vo štvorci. Ak si myslíš, že si objekt už poskladal stlač tlačidlo koniec. Na konci hry sa ti zobrazí tvoje skóre z posledných desitich hier.";
  div.setAttribute('class', 'napoveda');

  document.body.appendChild(div);

  g=1;}
  else {$('.napoveda').remove();
        g=0;}

  

};

var myVar=0;

function myFunction(){
  
 
  myVar = setInterval(myTimer, 1000);

}

function start(){
  location.reload();
}

function koniec(){
top_score=localStorage.getItem('score');

   if(!localStorage || !top_score) { 
     top_score = 0;
   }

  skore();
  if(top_score>0){
  document.getElementById('vysled_skore').innerHTML=top_score;}
  localStorage.setItem('score', top_score);
  console.log(top_score);
  clearTimeout(myVar);
}

function skore(){

if(score>top_score){
top_score=score;
 }
}

function myTimer() {
    
  score--;
  if(score==0){
  clearTimeout(myVar);}
  document.getElementById('result').innerHTML=score;
    
}



