import { ThirdAngularPage } from './app.po';

describe('third-angular App', () => {
  let page: ThirdAngularPage;

  beforeEach(() => {
    page = new ThirdAngularPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
