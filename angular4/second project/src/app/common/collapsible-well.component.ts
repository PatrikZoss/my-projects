/**
 * Created by pzoss on 21-Jul-17.
 */
import {Component, Input} from "@angular/core";
@Component({
  selector:'collapsible-well',
  template:`
  <div (click)="toggleContent()" class="well" pointable>
    <h4>
      <ng-content select="[well-title]"></ng-content>
    </h4>
    <ng-content *ngIf="visible" select="[well-body]"></ng-content>
  </div>
  `,
  styles:[
    `
      .well{color: black;background: silver;cursor: pointer}
      .well:hover{background: orangered;color: cornsilk}
    `
  ]
})

export class CollapsibleWellComponent{
  visible:boolean=true;

  toggleContent(){
  this.visible=!this.visible;
  }

}
