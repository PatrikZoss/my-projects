import {ProfileComponent} from "./profile.component";
import {LoginComponent} from "./login.component";
/**
 * Created by pzoss on 19-Jul-17.
 */

export const userRoutes = [
  {path: 'profile', component: ProfileComponent},
  {path: 'login', component: LoginComponent}
]
