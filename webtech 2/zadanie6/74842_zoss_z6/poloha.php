<!DOCTYPE html>
<html>
<head>
	<title>Informácie o polohe</title>
	<meta charset="utf-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body style="background-image: url(bg2.jpg);">

<h1 style="text-align: center;font-family: 'Bungee Inline', cursive;color: #59fd50;">Poloha</h1>



<div id="informacie">
<div id="IP">Tvoja IP adresa je:</div>
<div id="gps2">GPS suradnice su:</div>
<div id="lat">latitude:</div>
<div id="long">longitude:</div>
<div id="position">Tvoja pozicia je:</div>
<div id="country">Krajina je:</div>
<div id="capital">Hlavné mesto tvojej krajiny je:</div>
<a href="index.php" class="btn btn-warning">Počasie</a>
<a href="statistika.php" class="btn btn-primary">Štatistika</a>
</div>
<script src="myscript.js"></script>
</body>
</html>