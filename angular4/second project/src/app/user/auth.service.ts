/**
 * Created by pzoss on 20-Jul-17.
 */
import {Injectable} from "@angular/core";
import {IUser} from "./user.modul";

@Injectable()
export class AuthService{
  currentUser:IUser
  loginUser(userName:string,password:string){
    this.currentUser = {
        id:1,
        userName:userName,
        firstName:'John',
        lastName: 'Papa'
    }
  }

  updateCurrentUser(firstName:string,lastName:string){
    this.currentUser.firstName=firstName
    this.currentUser.lastName=lastName
  }

  isAuthenticated(){
    return !!this.currentUser;
  }
}
