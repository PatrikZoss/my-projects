var currentState;
var webWorker;
var computeButton;

window.onload = function() {
  currentState = document.getElementById("state");
  computeButton = document.getElementById("computeButton");
};

function computePrime() {
  computeButton.disabled = true;

  var fromNumber = document.getElementById("from").value;
  var toNumber = document.getElementById("to").value;

  webWorker = new Worker("workersPrime.js");
  webWorker.onmessage = getMessage;
  webWorker.onerror = errorHandler;
  
  webWorker.postMessage(
   { from: fromNumber,
     to: toNumber
   }
  );

  currentState.innerHTML = "Hľadajú sa prvočísla v rozsahu od("+
   fromNumber + " do " + toNumber + ") ...";  
}

function getMessage(event) {
  var message = event.data;

  if (message.messageType == "PrimeList") {
    var primes = message.data;

    var primeList = "";
    for (var i=0; i<primes.length; i++) {
      primeList += primes[i];
      if (i != primes.length-1) primeList +=  " * ";
    }
    var displayList = document.getElementById("OutputBox");
    displayList.innerHTML = primeList;

    if (primeList.length == 0) {
      currentState.innerHTML = "Chyba! Skontrolujte rozsah a skúste znova";
    }
    else {
      currentState.innerHTML = "Výpočet úspešne vykonaný";
    }
    computeButton.disabled = false;
  }
  else if (message.messageType == "Progress") {
    currentState.innerHTML = "Úspešne vykonaných percent "+ message.data + "% z výpočtu ";
  }
}

function errorHandler(error) {
  currentState.innerHTML = error.message;
}

function TerminateCompute() {
  webWorker.terminate();
  webWorker = null;
  currentState.innerHTML = "";
  computeButton.disabled = false;
}