-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: localhost
-- Čas generovania: St 15.Mar 2017, 21:51
-- Verzia serveru: 5.7.17-0ubuntu0.16.04.1
-- Verzia PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `zadanie2`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `login`
--

CREATE TABLE `login` (
  `id` int(3) NOT NULL,
  `log_uz` varchar(40) COLLATE utf8_slovak_ci NOT NULL,
  `cas` timestamp NULL DEFAULT NULL,
  `sposob_prihl` varchar(40) COLLATE utf8_slovak_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Sťahujem dáta pre tabuľku `login`
--

INSERT INTO `login` (`id`, `log_uz`, `cas`, `sposob_prihl`) VALUES
(51, 'patrik@patrik.patrik', '2017-03-15 19:32:19', 'registacia'),
(52, 'patrik@patrik.patrik', '2017-03-15 19:32:46', 'registacia'),
(53, 'patrik@patrik.patrik', '2017-03-15 19:38:21', 'registracia'),
(54, 'patrik@patrik.patrik', '2017-03-15 19:39:44', 'registracia'),
(55, 'patrik@patrik.patrik', '2017-03-15 19:49:13', 'registracia'),
(56, 'patrik@patrik.patrik', '2017-03-15 19:49:31', 'registracia'),
(66, 'patrik@patrik.patrik', '2017-03-15 20:00:51', 'registracia'),
(67, '74842@is.stuba.sk', '2017-03-15 20:01:07', 'ldap'),
(80, 'patrik@patrik.patrik', '2017-03-15 20:07:41', 'registracia'),
(89, 'zosspatrik@gmail.com', '2017-03-15 20:19:26', 'google'),
(91, 'michal.michna12@gmail.com', '2017-03-15 20:20:12', 'google'),
(92, 'michal.michna12@gmail.com', '2017-03-15 20:20:26', 'google'),
(93, 'zosspatrik@gmail.com', '2017-03-15 20:20:42', 'google'),
(97, 'zosspatrik@gmail.com', '2017-03-15 20:22:18', 'google'),
(99, 'patrik@patrik.patrik', '2017-03-15 20:22:57', 'registracia'),
(113, 'michal.michna12@gmail.com', '2017-03-15 20:34:46', 'google'),
(116, 'michal.michna12@gmail.com', '2017-03-15 20:35:12', 'google'),
(117, 'michal.michna12@gmail.com', '2017-03-15 20:35:18', 'google'),
(118, 'michal.michna12@gmail.com', '2017-03-15 20:35:30', 'google'),
(120, 'michal.michna12@gmail.com', '2017-03-15 20:36:35', 'google'),
(121, 'michal.michna12@gmail.com', '2017-03-15 20:37:03', 'google'),
(143, 'michal.michna12@gmail.com', '2017-03-15 20:47:22', 'google'),
(144, 'patrik@patrik.patrik', '2017-03-15 20:50:30', 'registracia');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `pouzivatel`
--

CREATE TABLE `pouzivatel` (
  `id` int(4) NOT NULL,
  `meno` varchar(40) COLLATE utf8_slovak_ci NOT NULL,
  `priezvisko` varchar(40) COLLATE utf8_slovak_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_slovak_ci NOT NULL,
  `login` varchar(40) COLLATE utf8_slovak_ci NOT NULL,
  `heslo` varchar(60) COLLATE utf8_slovak_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Sťahujem dáta pre tabuľku `pouzivatel`
--

INSERT INTO `pouzivatel` (`id`, `meno`, `priezvisko`, `email`, `login`, `heslo`) VALUES
(47, 'patrik', 'zoss', 'patrik@patrik.patrik', 'patrik', '$2y$12$1qSaNIcRWs7W.Yg0lND9duFFd6q.Nla9fw1nnzDz6PoROLTwklmxK'),
(50, 'Patrik', 'Zoss', '74842@is.stuba.sk', 'xzoss', 'ec0e0322eec46f3b3cf66b432ee72ef5'),
(51, 'Miso', 'Michna', 'miso@miso.miso', 'miso', '$2y$12$FkL50QMNg9GdjFhPsdWfcOCy27emSZqOgkdhfysUTDjKAW3qcewIK'),
(52, 'dasdas', 'dsada', 'dusan@dusan.dusan', 'dusan', '$2y$12$2kaKVNga2fOukMZlb0f69.mfFrdPZxrLut3e1MggBpdFhin8zuJ0u'),
(53, 'dasda', 'sada', 'dsadada', 'dusko', '$2y$12$sRLZOGWHzMFJeX.M191cPOP7KPOBob5MXa/RCx18bCSWGxWGPWOPS'),
(55, 'Patrik', '', 'zosspatrik@gmail.com', 'zosspatrik@gmail.com', NULL),
(56, '', '', '', '', NULL),
(57, 'Michal', '', 'michal.michna12@gmail.com', 'michal.michna12@gmail.com', NULL);

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`),
  ADD KEY `log_uz` (`log_uz`);

--
-- Indexy pre tabuľku `pouzivatel`
--
ALTER TABLE `pouzivatel`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pre exportované tabuľky
--

--
-- AUTO_INCREMENT pre tabuľku `login`
--
ALTER TABLE `login`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT pre tabuľku `pouzivatel`
--
ALTER TABLE `pouzivatel`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
