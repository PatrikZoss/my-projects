/**
 * Created by pzoss on 20-Jul-17.
 */
import {Component} from "@angular/core";
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";

@Component({
templateUrl:'login.component.html',
  styles:[
    `  .form-group{color: orangered}
        em{float: right;color: #E05C65;padding-left: 10px}
    `
  ]
})

export class LoginComponent{

  constructor(private authService:AuthService,private router:Router){

  }

  login(formValues){
    this.authService.loginUser(formValues.userName,formValues.password)
    this.authService.updateCurrentUser(formValues.userName,'')
    this.router.navigate(['events'])
  }

  cancel(){
    this.router.navigate(['events'])
  }

}
