<!DOCTYPE html>
<html>
<head>
	<title>Webservices</title>
	<meta charset="utf-8">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Spirax" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
</head>
<body>

<h1>Webservices - REST</h1>

<form action="#" method="GET">
<label for="usr">Zadajte datum:</label><br>
<input type="text" id="vstup1" name="datum" placeholder="Datum vo formate MMDD" class="zadaj1" required>
</form>

<button id="tlacitko1" class="btn btn-primary btn-sm">Send!</button>


<form action="#" method="GET">
<label for="usr">Zadajte meno a štát:</label><br>
<input type="text" id="vstup2" name="name" placeholder="Meno..." class="zadaj1" required>
<select name="state" id="vstup3" class="droptd">
    <option value="SK" selected>SK</option>
    <option value="CZ">CZ</option>
    <option value="HU">HU</option>
    <option value="PL">PL</option>
    <option value="AT">AT</option>
</select>
<!-- <input type="text" name="state" placeholder="Štát ..." class="zadaj1"> -->
</form>
<button id="tlacitko2" class="btn btn-success btn-sm">Send!</button>

<form action="#" method="GET">
<select name="sviatky" id="vstup4" class="droptd">
    <option value="SKsviatky" selected>SK sviatky</option>
    <option value="CZsviatky">CZ sviatky</option>
    <option value="SKdni">SK dni</option>
</select>
</form>
<button id="tlacitko3" class="btn btn-danger btn-sm">Send!</button>


<form action="#" method="POST">
<label for="usr">Zadajte meno, aké chcete vložiť a v ktorý deň:</label><br>
<input type="text" name="meno" id="vstup5" placeholder="Meno..." class="zadaj1" required>
<input type="text" name="den" id="vstup6" placeholder="Deň..." class="zadaj1" required>
</form>
<button id="tlacitko4" class="btn btn-warning btn-sm">Insert!</button>

<div id="result" class="result"></div>

<script src="script.js"></script>
</body>
</html>