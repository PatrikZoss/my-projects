var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
var socket = new WebSocket('ws://147.175.98.235:5500');

var radius = 10;
var dragging = false;

canvas.with=window.innerWidth;
canvas.height=window.innerHeight;

context.lineWidth=radius*2;

var putPoint=function(e){
	if (dragging) {
	context.lineTo(e.clientX,e.clientY);
	context.stroke();	
	context.beginPath();
	//context.arc(e.offsetX,e.offsetY,radius,0,Math.PI*2);
	context.arc(e.clientX,e.clientY,radius,0,Math.PI*2);
	context.fill();
	context.beginPath();
	context.moveTo(e.clientX,e.clientY);

	var data = {
		"x": e.clientX,
		"y": e.clientY, 
	};
	socket.send(JSON.stringify(data));

}
}

socket.onmessage = function (message) {
    var data = JSON.parse(JSON.parse(message.data).utf8Data);
    console.log(data);

    context.lineTo(data.x,data.y);
	context.stroke();	
	context.beginPath();
	//context.arc(e.offsetX,e.offsetY,radius,0,Math.PI*2);
	context.arc(data.x,data.y,radius,0,Math.PI*2);
	context.fill();
	context.beginPath();
	context.moveTo(data.x,data.y);
    
};

socket.onerror = function (error) {
    console.log('WebSocket error: ' + error);
};

var engage = function(e){
dragging=true;
putPoint(e);
}

var disengage = function(){
dragging=false;
context.beginPath();
}

canvas.addEventListener('mousedown',engage);
canvas.addEventListener('mousemove',putPoint);
canvas.addEventListener('mouseup',disengage);


$("#del").click(function(){
context.clearRect(0, 0, canvas.width, canvas.height);
});


$("#save").click(function(){

var obrazok=canvas.toDataURL("image/png").replace("image/png","image/octet-stream");
document.getElementById("save").setAttribute("href",obrazok);

});