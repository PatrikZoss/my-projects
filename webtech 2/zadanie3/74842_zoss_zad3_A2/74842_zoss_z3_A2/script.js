<script type="application/javascript">
    var content = document.getElementById('content');
    var socket = new WebSocket('ws://147.175.98.105:5500');

    socket.onmessage = function (message) {
        var data = JSON.parse(JSON.parse(message.data).utf8Data);
        console.log(data);
        $( "table tr:nth-child("+(data.row+1)+") td:nth-child("+(data.col+1)+")" ).css("background-color", "blue");
    };

    socket.onerror = function (error) {
        console.log('WebSocket error: ' + error);
    };

    $('td').click(function() {
        $(this).css('backgroundColor', 'red');

        var col = this.cellIndex;
        var tr = $(this).closest('tr');
        var row = tr.index();

        console.log(row + " " + col);
        var data = {"row": row, "col": col};
        socket.send(JSON.stringify(data));
    });
