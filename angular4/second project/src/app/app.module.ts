import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {NavBarComponent} from "./nav/navbar.component";
import {RouterModule} from "@angular/router";
import {appRoutes} from "./routes";
import {Error404Component} from "./errors/404.component";

import {
  EventsListComponent,
  EventThumbnailComonent,
  EventService,
  EventDetailComponent,
  CreateEventComponent,
  EventRouteActivator,
  UpvoteComponent,
  VoterService,
  LocationValidator,
  CreateTableComponent,
  EventListResolver,
} from './events/index'

import {AuthService} from "./user/auth.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SessionListComponent} from "./events/event-details/session-list.component";
import {CreateSessionComponent} from "./events/event-details/create-session.component";
import {JQ_TOKEN,CollapsibleWellComponent, SimpleModalComponent,ModalTriggerDirective} from './common/index'
import {DurationPipe} from "./events/shared/duration.pipe";
import {JQueryStyleEventEmitter} from "rxjs/observable/FromEventObservable";
import {HttpModule} from "@angular/http";

declare let jQuery : Object;


@NgModule({
  declarations: [
    AppComponent,
    EventsListComponent,
    EventThumbnailComonent,
    NavBarComponent,
    CreateEventComponent,
    EventDetailComponent,
    Error404Component,
    CreateTableComponent,
    CreateSessionComponent,
    SessionListComponent,
    SimpleModalComponent,
    UpvoteComponent,

    LocationValidator,
    ModalTriggerDirective,
    CollapsibleWellComponent,
    DurationPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    EventService,
    EventRouteActivator,
    EventListResolver,
    VoterService,
    AuthService,
    {provide: JQ_TOKEN,useValue:jQuery},
    {provide:'canDeactivatecreateEvent',useValue:checkDirtyState}
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }


function checkDirtyState(component:CreateEventComponent){
  if (component.isDirty){
    return window.confirm('You have not saved this event, do you really want to cancel?')
  }
  return false
}
