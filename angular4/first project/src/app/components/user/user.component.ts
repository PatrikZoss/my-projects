import { Component, OnInit } from '@angular/core';
import {DataService} from '../../services/data.service';
import {subscribeOn} from "rxjs/operator/subscribeOn";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
name:string;
age:number;
email:string;
address:Address;
hobbies:string[];
hello:any;
posts:Post;
isEdit:boolean=false;

  constructor(private dataService:DataService) {

    console.log('constuctor ran...');
  }

  ngOnInit() {
    console.log('ngOninit ran...');
    this.name='John Doe';
    this.age=30;
    this.email='test@test.com';
    this.address={
      stret:'50 Main st.',
      city:' Boston, ',
      state:'MA'
    }
    this.hobbies=['write code','watch movies','listen to music'];
    this.dataService.getPosts().subscribe((posts)=>{
      // console.log(posts);
      this.posts=posts;

  });
  }
onClick(){
    this.name='Patrik Zoss';
    this.hobbies.push('New hobby');
}
addHobby(hobby){
console.log(hobby);
this.hobbies.unshift(hobby);
return false;
}
deleteHobby(hobby){
for(let i=0;i<this.hobbies.length;i++){
  if(this.hobbies[i]==hobby){
    this.hobbies.splice(i,1);
  }
}
}

toggleEdit(){
  this.isEdit=!this.isEdit;
}

}

interface Address{
  stret:string;
  city:string;
  state:string;
}

interface Post{
  id:number;
  title:string;
  body:string;
  userId:number;
}
