<!DOCTYPE html>
<html>
<head>

	<title>Štatistika</title>
	<meta charset="utf-8">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Bungee+Inline" rel="stylesheet">
</head>
<body>

<h1 style="text-align: center; font-family: 'Bungee Inline', cursive;">Štatistika</h1>

<div id="vypis_tab"></div>
<div class="col-md-6">
<div id="map" style="width: 500px; height: 500px "></div>
</div>

<div>Počet návštevnikov v danom intervale:</div>
<div id="navsevnost"></div>


<!-- Modal -->
<div id="myModal" class="modal fade universal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">X</button>
        <h4 class="modal-title" id="krajina"></h4>
      </div>
      <div class="modal-body">
      <div id="tabulka2"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<a href="index.php" class="btn btn-warning">Počasie</a>
<a href="poloha.php" class="btn btn-danger">Poloha</a>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAilKK4FCbAgib5r4147vFvHzO4sHAtQcI"></script>

<script src="myscript.js"></script>
</body>
</html>