var pole_suradnic1 = [];
var pole_suradnic2 = [];
var start = true, end = false;
var source;
var plot1;
var graphSin = true, graphCos = true;
if (start) {
    window.addEventListener("load", nacitavajSuradnice());
    start = false;
}
function nacitavajSuradnice() {

    if (typeof (EventSource) !== "undefined") {
        source = new EventSource("http://vmzakova.fei.stuba.sk/sse/sse.php");
        source.addEventListener("message", function (e) {
            var data = JSON.parse(e.data);
            var axis_x = data.x;
            var axis_y1 = data.y1;
            var axis_y2 = data.y2;
            pole_suradnic1.push([axis_x, axis_y1]);
            pole_suradnic2.push([axis_x, axis_y2]);
            plot1 = $.jqplot('div_graph', [pole_suradnic1, pole_suradnic2],
                    {title: 'Graf pre zašumený sínus a kosínus',
                        axes: {
                            yaxis: {min: -1.5, max: 1.5,
                                tickOptions: {
                                    formatString: '%.2f'
                                }},
                            xaxis: {min: 0, max: 120,
                                tickOptions: {
                                    formatString: '%d'
                                }}},
                        legend: {
                            labels: ['Sínus', 'Kosínus'],
                            renderer: $.jqplot.EnhancedLegendRenderer,
                            show: true,
                            zoom: true

                        },
                        series: [{color: 'green',
                                markerOptions: {size: 3, style: "+"},
                                lineWidth: 1},
                            {color: 'black',
                                markerOptions: {size: 3, style: "+"},
                                lineWidth: 1}
                        ]
                    });
            plot1.series[0].show = graphSin;
            plot1.series[1].show = graphCos;
            plot1.replot();
            if (axis_x == 120) {
                source.close();
                end = true;
            }
        }, false);
    } else {
        document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
    }
}
;
function checkCheckBox() {
    var sinusBox = document.getElementById("sin").checked;
    var cosinusBox = document.getElementById("cos").checked;
    if (sinusBox) {
        graphSin = plot1.series[0].show = true;
    } else {
        graphSin = plot1.series[0].show = false;
    }
    if (cosinusBox) {
        graphCos = plot1.series[1].show = true;
    } else {
        graphCos = plot1.series[1].show = false;
    }
    plot1.replot();


}
var animate = true;
function plotGraph() {
    source.close();
    for (var i = 0; i < pole_suradnic1.length; i++) {
        plot1 = $.jqplot('div_graph', [pole_suradnic1, pole_suradnic2],
                {animate: true,
                    animateReplot: animate,
                    title: 'Zašumený sínus a kosínus',
                    axes: {
                        yaxis: {min: -1.5, max: 1.5,
                            tickOptions: {
                                formatString: '%.2f'
                            }},
                        xaxis: {min: 0, max: pole_suradnic1.length - 1,
                            tickOptions: {
                                formatString: '%.2f'
                            }}},
                    cursor: {
                        show: true,
                        tooltipLocation: 'sw',
                        zoom: true
                    },
                    legend: {
                        labels: ['Sínus', 'Kosínus'],
                        renderer: $.jqplot.EnhancedLegendRenderer,
                        show: true,
                        zoom: true

                    },
                    series: [{color: 'green',
                            markerOptions: {size: 3, style: "x"},
                            lineWidth: 1,
                            rendererOptions: {
                                smooth: true
                            }},
                        {color: 'black',
                            markerOptions: {size: 3, style: "x"},
                            lineWidth: 1,
                            rendererOptions: {
                                smooth: true
                            }}
                    ]
                });
        plot1.series[0].show = graphSin;
        plot1.series[1].show = graphCos;
        plot1.replot();
    }
    animate = false;
    plot1.animateReplot = animate;
    end = true;
}
function resetZoom() {
    plot1.resetZoom();
}

