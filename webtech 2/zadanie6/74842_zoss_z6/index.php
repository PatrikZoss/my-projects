<!DOCTYPE html>
<html>
<head>
	<title>Počasie</title>
	<meta charset="utf-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Bungee+Inline" rel="stylesheet">
</head>
<body style="background-image: url(bg.jpg);">

<h1 style="text-align: center;font-family: 'Bungee Inline', cursive;">Počasie</h1>

<div class="pocasie">
<div id="poloha">Vaša poloha je:</div>
<div id="ip">Vaša IP adreasa je:</div>
<div id="teplota">Teplota je:</div>
<div id="minteplota">Min teplota je:</div>
<div id="maxteplota">Max teplota je:</div>
<div id="vlhkost">Vlhkosť je:</div>
<div id="tlak">Tlak je:</div><br>

<div id="oblacnost">Oblačnosť je:</div>
<div id="vychod">Východ slnka je:</div>
<div id="zapad">Západ slnka je:</div>
<div id="rychlost">Rýchlosť vetru:</div>
<div id="orientacia">Orientácia vetra:</div>

<a href="poloha.php" class="btn btn-danger" id="tlacitko1">Poloha</a>
<a href="statistika.php" class="btn btn-success" >Štatistika</a>
</div>
<script src="myscript.js"></script>
</body>
</html>