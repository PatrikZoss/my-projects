import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {BlogComponent} from './blog/blog.component';
import {AppComponent} from './app.component';

export const router: Routes = [
{   path: 'blog', component: BlogComponent},
  { path: 'home', component: AppComponent}
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
