import { FifthAngularPage } from './app.po';

describe('fifth-angular App', () => {
  let page: FifthAngularPage;

  beforeEach(() => {
    page = new FifthAngularPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
