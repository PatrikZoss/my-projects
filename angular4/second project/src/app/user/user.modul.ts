/**
 * Created by pzoss on 20-Jul-17.
 */
export interface IUser{
  id:number
  firstName:string
  lastName:string
  userName:string
}
