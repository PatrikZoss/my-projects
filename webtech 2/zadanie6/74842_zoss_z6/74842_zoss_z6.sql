-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: localhost
-- Čas generovania: Po 01.Máj 2017, 21:37
-- Verzia serveru: 5.7.17-0ubuntu0.16.04.1
-- Verzia PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `zadanie6`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `navstevnici`
--

CREATE TABLE `navstevnici` (
  `id` int(11) NOT NULL,
  `ip` varchar(30) COLLATE utf8_slovak_ci NOT NULL,
  `cas` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mesto` varchar(50) COLLATE utf8_slovak_ci NOT NULL,
  `krajina` varchar(50) COLLATE utf8_slovak_ci NOT NULL,
  `pocasie` int(11) NOT NULL,
  `countrycode` varchar(10) COLLATE utf8_slovak_ci NOT NULL,
  `poloha` int(11) NOT NULL,
  `statistika` int(11) NOT NULL,
  `lang` double NOT NULL,
  `lon` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Sťahujem dáta pre tabuľku `navstevnici`
--

INSERT INTO `navstevnici` (`id`, `ip`, `cas`, `mesto`, `krajina`, `pocasie`, `countrycode`, `poloha`, `statistika`, `lang`, `lon`) VALUES
(1, '147.175.184.161', '2017-05-01 12:43:57', 'Bratislava', 'Slovak Republic', 0, 'SK', 0, 0, 48.15, 17.1167),
(2, '147.175.184.161', '2017-05-01 12:50:53', 'Bratislava', 'Slovak Republic', 0, 'SK', 0, 0, 48.15, 17.1167),
(3, '147.175.178.168', '2017-05-01 13:03:47', 'Bratislava', 'Slovak Republic', 0, 'SK', 0, 0, 48.15, 17.1167),
(4, '147.175.181.79', '2017-05-01 18:35:13', 'Bratislava', 'Slovak Republic', 0, 'SK', 0, 0, 48.15, 17.1167),
(5, '147.175.186.234', '2017-05-01 19:27:51', 'Bratislava', 'Slovak Republic', 0, 'SK', 0, 0, 48.15, 17.1167);

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `navstevnici`
--
ALTER TABLE `navstevnici`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pre exportované tabuľky
--

--
-- AUTO_INCREMENT pre tabuľku `navstevnici`
--
ALTER TABLE `navstevnici`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
