/**
 * Created by pzoss on 19-Jul-17.
 */
import {Component, OnInit} from "@angular/core";
import {EventService} from "../shared/event.service";
import {ActivatedRoute, Params} from  '@angular/router'
import {IEvent} from '../shared/index'
import {ISession} from "../shared/event.model";

@Component({
templateUrl: 'event-details.component.html',
  styles:[`
    h2{color: orangered}
    a{cursor: pointer}
    .container{padding-left: 20px;padding-right: 20px;color: silver;}
    .event-image{height: 100px}
    .btn-success{margin-bottom: 5px;background-color: silver;color: black}
    .btn-success:hover{background-color: orangered;color: white}
  `]
})

export class EventDetailComponent implements OnInit{
  event:IEvent
  addMode:boolean
  filterBy:string = 'all';
  sortBy: string = 'votes';

  constructor(private eventService: EventService, private route : ActivatedRoute){  }

  ngOnInit(){
    this.route.params.forEach((params:Params)=>{
      this.event=this.eventService.getEvent(+params['id'])
      this.addMode=false;
    })
  }

  addSession(){
    this.addMode=true
  }

  cancelAddSession(){
    this.addMode=false
  }

  saveNewSession(session:ISession){
   const nextId=Math.max.apply(null,this.event.sessions.map(s=>s.id));
   session.id=nextId+1
   this.event.sessions.push(session)
   this.eventService.updateEvent(this.event)
    this.addMode=false
  }

}
