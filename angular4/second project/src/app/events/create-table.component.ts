import {Component, OnInit} from "@angular/core";
import {IEvent} from "./shared/event.model";
import {EventService} from "./shared/event.service";
import {ActivatedRoute, Params} from "@angular/router";


@Component({
  templateUrl:"create-table.component.html",
  styles:[`
  thead{color: orangered;background-color: silver}
  tfoot{color: orangered;background-color: silver}
  body{color: orangered}
  `]
})
export class CreateTableComponent implements OnInit{
  event:IEvent
  constructor(private eventService:EventService, private route : ActivatedRoute){}

  ngOnInit(){
    this.route.params.forEach((params:Params)=>{
      this.event=this.eventService.getEvent(+params['id']);
    })
  }
}
