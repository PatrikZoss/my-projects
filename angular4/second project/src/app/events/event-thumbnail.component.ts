/**
 * Created by pzoss on 18-Jul-17.
 */
import {Component, Input, Output, EventEmitter} from "@angular/core";
import { IEvent } from './shared/index'

@Component({
  selector:'event-thumbnail',
  template:`
    <div [routerLink]="['/events',event.id]" class="thumbnail">
      <h2>{{event?.name | uppercase}}</h2>
      <div>Date: {{event?.date | date : 'shortDate'}}</div>
      <div [ngStyle]="{'color':event?.time==='8:00 am' ? 'green' : 'black', 'font-weight': event?.time  === '8:00 am' ? 'bold' : 'normal'  }" 
           [ngSwitch]="event?.time">Time: {{event?.time}}
        <span *ngSwitchCase="'8:00 am'">(Early start)</span>
        <span *ngSwitchDefault>(Normal start)</span>
        <span *ngSwitchCase="'10:00 am'">(Late start)</span>
      </div>
      <div>Price: {{event?.price | currency:'USD':true}}</div>
      <div *ngIf="event?.location">
        <span>Location: {{event?.location?.address}}</span>
        <span class="pad-left">{{event?.location?.city}}, {{event?.location?.country}}</span>
      </div>
      <div *ngIf="event?.onlineUrl">
        Online Url: {{event?.onlineUrl}}
      </div>
    </div>`,
  styles:[`
    .green{ color: green !important;}
    .thumbnail{min-height: 210px;color: black;background: silver;cursor: pointer}
    .thumbnail:hover{background: orangered;color: cornsilk}
    .pad-left{margin-left: 10px;}
  `]
})

export class EventThumbnailComonent{
@Input() event:IEvent

  getStartTimeClass(){
  const isEarlyStart = this.event && this.event.time === '8:00 am'
    return {green: isEarlyStart, bold: isEarlyStart}
  }
}
