var swatches = document.getElementsByClassName('swatch');
for (var i = 0,n=swatches.length; i<n;i++) {
	swatches[i].addEventListener('click',setSwatch);
}

function setColor(color){
	context.fillStyle=color;
	context.strokeStyle=color;
	var active = document.getElementsByClassName('active')[0];
	if (active) {
	active.className='swatch';
	}
}

function setSwatch(e){
//identify swatch
var swatch=e.target;
//set color
setColor(swatch.style.backgroundColor);
//give active class
swatch.className += ' active';
}