<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Websockets</title>
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Griffy" rel="stylesheet">
</head>

<body style="margin: 0">

<h1>Drawing with websockets</h1>
<div class="tlacitka"></div>


 <div id="t">
        <a href="#" id="save" download="snimka.png" class="btn btn-primary btn-lg">  Save 
        <!-- <button type="button" class="btn btn-primary btn-lg" id="sav">Save</button>
 -->        </a>
<button type="button" class="btn btn-danger btn-lg" id="del">Delete</button>
    </div>

    <div class="toolbar" id="toolbar">
    	<div id="rad">
    		Radius <span id="radval">10</span>
    		<div id="decrad" class="radcontrol">-</div>
    		<div id="incrad" class="radcontrol">+</div>	
    	</div>

    	<div id="colors" >
    		<div class="swatch active" style="background-color: red;"></div>
    		<div class="swatch" style="background-color: green;"></div>
    		<div class="swatch" style="background-color: blue;"></div>
    	</div>
    </div>

<canvas id="canvas" width="700" height="100"></canvas>
 <div id="ahoj"></div>

 <!-- width="200" height="100"
 -->

<script src="myscript.js"></script>
<script src="radius.js"></script>
<script src="colors.js"></script>
</body>
</html>